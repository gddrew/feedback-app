import React from 'react';
import { createRoot } from 'react-dom/client';
import './index.css';
import App from './App';

const container = document.getElementById('root');
const root = createRoot(container);
root.render(
  // <React.StrictMode>
  <App />
  // </React.StrictMode>
);

/**
 * Removed strict mode. Running React in Strict mode renders your app twice on every render, but Framer motion removes animates elements with requestAnimationFrame, outside of React for performance. The two seem a little mismatched in StrictMode.
 */
